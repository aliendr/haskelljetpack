module GlobalStateHandlers
  ( withDeath,
    withDeathStats,
    withScaledDraw,
  )
where

import Graphics.Gloss.Interface.IO.Game
import Screen


type GameOf state =
  state ->
  (state -> Picture) ->
  (Event -> state -> state) ->
  (Float -> state -> state) ->
  IO ()

data GameStatus a = Alive a | Dead a

-- | Provides a Death state to the game and a Death screen
-- with restart
withDeath :: (a -> Bool) -> GameOf (GameStatus a) -> GameOf a
withDeath isAlive = withDeathStats isAlive (const [])


-- | Provides a Death state to the game
-- and a death screen with statistics and restart
withDeathStats :: (a -> Bool) -> (a -> [String]) -> GameOf (GameStatus a) -> GameOf a
withDeathStats isAlive getStats wrapper initial draw input time =
  wrapper (Alive initial) newDraw newInput newTime
  where
    -- Just call the old draw when alive
    newDraw (Alive a) = draw a 
    -- And draw the death screen when dead
    newDraw (Dead a) =
      color (light black) (rectangleSolid screenWidth screenHeight)
        <> (color white $ scale 0.7 0.7 $ text "You died")
        <> ( translate 0 (-50) $
               color white $
                 scale 0.3 0.3 $ text "Press Enter"
           )
        -- And stats
        <> ( translate 0 (-150) $
               color white $
                 scale 0.2 0.2 $
                   pictures $
                     map text $ take 1 $ getStats a
           )

    -- redirect input or restart game on Enter when dead
    newInput event (Alive a) = Alive (input event a)
    newInput (EventKey (SpecialKey KeyEnter) Down _ _) (Dead _) = Alive initial
    newInput _ (Dead a) = Dead a

    newTime _dt (Dead a) = (Dead a)
    newTime dt (Alive a) =
      if isAlive newState
        then Alive newState
        else (Dead a)
      where
        newState = time dt a



-- | Run a game with scaled by (scaleX, scaleY) draw 
withScaledDraw :: Float -> Float -> GameOf a -> GameOf a
withScaledDraw scaleX scaleY wrapper initial draw =
  wrapper initial newDraw
  where
    newDraw = (scale scaleX scaleY . draw)
