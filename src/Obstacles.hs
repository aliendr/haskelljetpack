module Obstacles where

import Geometry
import Graphics.Gloss

data Obstacle = LazerObstacle Lazer

-- | Move obstacle given offsets and obstacle to move
moveObstacle :: (Float, Float) -> Obstacle -> Obstacle
moveObstacle (offsetX, offsetY) (LazerObstacle (Lazer (px1, py1) (px2, py2) state time phase)) =
  LazerObstacle (Lazer (px1 + offsetX, py1 + offsetY) (px2 + offsetX, py2 + offsetY) state time phase)

-- update state of a lazer
updateObstacle :: Float -> Obstacle -> Obstacle
updateObstacle dt (LazerObstacle (Lazer p1 p2 _ time phase)) = LazerObstacle (Lazer p1 p2 newState updatedTime phase)
 where
   newTime = time + dt
  --  (newState, updatedTime) = 
  --    case newTime < phase of 
  --            True     ->  (True, newTime)
  --            False    ->  (False, 0)
   newState = if ( newTime < phase ) then True else False
   updatedTime = if ( newTime < 2 * phase ) then newTime else 0


data Orientation = Horizontal | Vertical
  deriving (Eq)

-- centers of circles, lenght of connecting line btw 2 circles and the state(on/off)
data Lazer = Lazer Point Point Bool  Float Float

-- radius of circle and width of connecting line btw 2 circles
lazerRadius :: Float
lazerWidth :: Float
(lazerRadius, lazerWidth) = (5, 5)

-- | Create a Lazer Obstacle between two points in the world
createLazer :: Point -> Point -> Bool -> Float -> Float -> Obstacle
createLazer p1 p2 state time phase = LazerObstacle (Lazer p1 p2 state time phase)

-- get collider of an obstacle
getObstacleCollider :: Obstacle -> Collider
getObstacleCollider (LazerObstacle lazer) = getLazerCollider lazer

-- get collider of lazer
getLazerCollider :: Lazer -> Collider
getLazerCollider (Lazer (px1, py1) (px2, py2) _ _ _) = BoxCollider (x1, y1) (x2, y2) 0
  where
    ((x1, y1), (x2, y2)) = getColliderCoords ((px1, py1), (px2, py2)) lazerRadius

-- calculates left down and right up points for collider
getColliderCoords :: (Point, Point) -> Float -> (Point, Point)
getColliderCoords (p1@(px1, py1), p2@(px2, py2)) colliderRadius
  | (getOrientation p1 p2) == Vertical && py1 < py2 = firstPointEarlier
  | (getOrientation p1 p2) == Horizontal && px1 < px2 = firstPointEarlier
  | otherwise = secondPointEarlier
  where
    firstPointEarlier = ((px1 - colliderRadius, py1 - colliderRadius), (px2 + colliderRadius, py2 + colliderRadius))
    secondPointEarlier = ((px2 - colliderRadius, py2 - colliderRadius), (px1 + colliderRadius, py1 + colliderRadius))

-- for now there are only Vertical and Horizontal lazers
getOrientation :: Point -> Point -> Orientation
getOrientation (px1, _py1) (px2, _py2)
  | px1 == px2 = Vertical
  | otherwise = Horizontal

-- draw obstacle
drawObstacle :: Obstacle -> Picture
drawObstacle (LazerObstacle lazer) = drawLazer lazer

-- draw lazer
drawLazer :: Lazer -> Picture
drawLazer (Lazer (px1, py1) (px2, py2) state time _) 
  | not state =  scale 0.5 0.5 (translate px1 py1 ( text (show (time))))  -- color black $ lazerLight orientation
  | otherwise = (translate px1 py1 lazerPoint)
    <> (translate px2 py2 lazerPoint)
    <> (color orange $ lazerLight orientation)
     where
      lazerPoint = color yellow $ circleSolid lazerRadius
      orientation = getOrientation (px1, py1) (px2, py2)
      lazerLight Vertical =
       polygon
        [ (px1 - lazerWidth / 2, py1),
          (px1 + lazerWidth / 2, py1),
          (px2 - lazerWidth / 2, py2),
          (px2 + lazerWidth / 2, py2)
        ]
      lazerLight Horizontal =
       polygon
        [ (px1, py1 - lazerWidth / 2),
          (px1, py1 + lazerWidth / 2),
          (px2, py2 - lazerWidth / 2),
          (px2, py2 + lazerWidth / 2)
        ]


