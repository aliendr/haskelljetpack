module Screen where

screenRatio :: Float
screenRatio = 16 / 9

screenMaxY :: Float
screenMaxY = 768 / 2

screenMaxX :: Float
screenMaxX = screenMaxY * screenRatio

screenHeight :: Float
screenHeight = 2 * screenMaxY

screenWidth :: Float
screenWidth = 2 * screenMaxX

-- | Screen bounds in format of ((left, bottom), (right, top))
screenBounds :: ((Float, Float), (Float, Float))
screenBounds = ((- screenMaxX, - screenMaxY), (screenMaxX, screenMaxY))
