{-# LANGUAGE OverloadedStrings #-}

module World where

import Geometry
import Graphics.Gloss
import Obstacles
import Screen
import System.Random

-- | Fantom types for Template data type
data Absolute

data Relative

-- | Part of the game a player has to pass through
data Template t = Template
  { templateBackground :: Picture,
    templateXCoordinate :: Float,
    templateLength :: Float,
    templateObstacles :: [Obstacle]
  }

-- | World state to be interacted
data WorldState = WorldState
  { worldTemplates :: [Template Relative]
    
  }

-- Templates initialization

-- | Creates a template given background, width, and list of obstacles
createTemplate :: Picture -> Float -> [Obstacle] -> Template Relative
createTemplate background xSize obstacles =
  Template
    { -- shifts to the right to start background at logical zero
      templateBackground = translate (xSize / 2) 0 background,
      templateXCoordinate = 0,
      templateLength = xSize,
      templateObstacles = obstacles
    }

-- | Initial template without obstacles to make user acquanted with gameplay
initialTemplate :: Template Relative
initialTemplate = createTemplate background xSize []
  where
    (xSize, ySize) = (screenWidth * 1.5, screenHeight)
    col = light aquamarine
    instruction = scale 0.3 0.3 $ text "Press Space to fly. Press Tab to immunity"
    background = color col (rectangleSolid xSize ySize) <> instruction

-- | Available templates to choose the next template from
sampleTemplates :: StdGen -> [Template Relative]
sampleTemplates g = zipWith3 createTemplate backgrounds xSizes lazers
  where
    transformPoint :: (Float, Float) -> Point -> Point
    transformPoint (xSize, ySize) (x, y) = (x * xSize, (y - 0.5) * ySize)
    createScaledLazer :: (Float, Float) -> ((Point, Point), (Bool, Float, Float))  -> Obstacle
    createScaledLazer (xSize, ySize) (((x1, y1), (x2, y2)), (lazerState, time, phase)) =
      createLazer (transformPoint (xSize, ySize) (x1, y1)) (transformPoint (xSize, ySize) (x2, y2)) lazerState time phase

    xSizes = [xSize1, xSize2, xSize3] -- width of templates
    backgrounds = [background1, background2, background3] -- backgounds of templates
    lazers = [lazers1, lazers2, lazers3] -- obstacles belong to templates
    --(xSize1, ySize1) = (screenWidth * 2, screenHeight)
    (xSize1, ySize1) = (screenWidth , screenHeight)

    
    background1 = color blue (rectangleSolid xSize1 ySize1)
    lazerPoints1 =
      [ ((0.1, 0.3), (0.1, 0.5)),
        ((0.2, 0.5), (0.2, 0.94)),
        ((0.25, 0.1), (0.25, 0.3)),
        ((0.4, 0.05), (0.4, 0.6)),
        ((0.52, 0.34), (0.52, 0.78))
        -- ((0.6, 0.2), (0.6, 0.4)),
        -- ((0.6, 0.56), (0.6, 0.68)),
        -- ((0.78, 0.12), (0.78, 0.34)),
        -- ((0.78, 0.73), (0.78, 0.96)),
        -- ((0.93, 0.06), (0.93, 0.5))
      ]

    lazers1 = map (createScaledLazer (xSize1, ySize1)) (zip lazerPoints1 (randomTimePhasesOfLazers g))


    --(xSize2, ySize2) = (screenWidth * 3, screenHeight)
    (xSize2, ySize2) = (screenWidth , screenHeight)

    
    background2 = translate 0 (-ySize2 / 4)
                  (color (dark red) (rectangleSolid xSize2 (ySize2 / 2)))
               <> translate 0 (ySize2 / 4)
                  (color (dark green) (rectangleSolid xSize2 (ySize2 / 2)))
               <> translate 0 0
                  (color white (rectangleSolid xSize2 (ySize2 / 7)))
    lazerPoints2 =
      [ ((0.04, 0.09), (0.16, 0.09)),
        ((0.24, 0.92), (0.38, 0.92)),
        ((0.12, 0.2), (0.24, 0.2)),
        ((0.15, 0.8), (0.33, 0.8)),
        ((0.3, 0.45), (0.6, 0.45))
        -- ((0.4, 0.72), (0.57, 0.72)),
        -- ((0.5, 0.87), (0.7, 0.87)),
        -- ((0.62, 0.13), (0.8, 0.13)),
        -- ((0.75, 0.6), (0.9, 0.6)),
        -- ((0.8, 0.8), (0.97, 0.8))
      ]

    lazers2 = map (createScaledLazer (xSize2, ySize2)) (zip lazerPoints2 (randomTimePhasesOfLazers g))

    --(xSize3, ySize3) = (screenWidth * 2.5, screenHeight)
    (xSize3, ySize3) = (screenWidth , screenHeight)

    
    background3 = color (light black) (rectangleSolid xSize3 ySize3)
    lazerPoints3 =
      [ ((0.04, 0.09), (0.16, 0.09)),
        ((0.2, 0.5), (0.2, 0.94)),
        ((0.12, 0.2), (0.24, 0.2)),
        ((0.4, 0.05), (0.4, 0.6)),
        ((0.3, 0.45), (0.6, 0.45))
        -- ((0.6, 0.2), (0.6, 0.4)),
        -- ((0.5, 0.87), (0.7, 0.87)),
        -- ((0.62, 0.13), (0.8, 0.13)),
        -- ((0.75, 0.6), (0.9, 0.6)),
        -- ((0.8, 0.8), (0.97, 0.8))
      ]

    lazers3 = map (createScaledLazer (xSize3, ySize3)) (zip lazerPoints3 (randomTimePhasesOfLazers g))

-- | Infinite list of templates
templatesList :: StdGen -> [Template Relative]
templatesList g = map ((!!) (sampleTemplates g)) indexes
  where
    indexes = randomRs (0, length (sampleTemplates g) - 1) g


-- generates list of state time phase for lazer
-- works not so random because the same generator used for for every template
randomTimePhasesOfLazers :: StdGen -> [(Bool, Float, Float)]
randomTimePhasesOfLazers g =  zip3 bools x y 
  where 
    x = randomRs (0, 18) g -- initial time
    y = randomRs (0, 13) g -- phase for which lazer will be on
    bools = [ odd m | m <- (randomRs (0, length (sampleTemplates g) - 1) g)] -- initial state


-- | Generate endless level using the StgGen random generator
initWorld :: StdGen -> WorldState
initWorld g = WorldState (initialTemplate : (templatesList g))

-- From relative to absolute

-- | Transform obstacles with coordinates relative to the template
--    given offset over X to obstacles with absolute coordinates
toAbsoluteObstacles :: Float -> [Obstacle] -> [Obstacle]
toAbsoluteObstacles offsetX = map (moveObstacle (offsetX, 0))

-- | change states of obstacles
updateStatesObstacles :: Float ->  [Obstacle] -> [Obstacle]
updateStatesObstacles dt = map (updateObstacle dt)


-- | Transform templates from relative to absolute representation given offset
toAbsolute :: Float -> [Template Relative] -> [Template Absolute]
toAbsolute _offsetX [] = []
toAbsolute offsetX (template : templates) = absoluteTemplate : toAbsolute (newX + len) templates
  where
    newX = templateXCoordinate template + offsetX
    len = templateLength template

    obstacles = templateObstacles template

    absoluteTemplate = template {templateXCoordinate = newX, templateObstacles = toAbsoluteObstacles newX obstacles}

-- Screen presentation

-- | Whether the template is on screen
onScreen :: Template Absolute -> Bool
onScreen template = x + len > - screenMaxX && x < screenMaxX
  where
    x = templateXCoordinate template
    len = templateLength template

-- | Templates that are on screen given list of relative templates
getOnScreenTemplates :: [Template Relative] -> [Template Absolute]
getOnScreenTemplates templates = takeWhile onScreen (dropWhile (not . onScreen) (toAbsolute (- screenMaxX) templates))




-- Drawing

-- | Render obstacles with absolute coordinates
drawTemplateObstacles :: [Obstacle] -> Picture
drawTemplateObstacles obstacles = pictures (map drawObstacle obstacles)

-- | Render template in absolute coordinates
drawTemplate :: Template Absolute -> Picture
drawTemplate template = translate offset 0 background <> drawTemplateObstacles obstacles
  where
    offset = templateXCoordinate template
    background = templateBackground template
    obstacles = templateObstacles template

-- | Render list of templates
drawTemplates :: [Template Relative] -> Picture
drawTemplates templates = pictures (map drawTemplate (getOnScreenTemplates templates))

-- | Draw current world that fits to the screen
drawWorld :: WorldState -> Picture
drawWorld (WorldState templates) =
  drawTemplates templates

-- Update world

-- | Update template given current horizontal speed and time passed
updateTemplate :: Float -> Float -> Template Relative -> Template Relative
updateTemplate speed dt template = template {templateXCoordinate = (x - dt * speed), templateObstacles = obstaclesWithUpdatedStates}
  where
    x = templateXCoordinate template
    obstaclesWithUpdatedStates = updateStatesObstacles dt (templateObstacles template)

-- update lazers in template relatives
updateTemplateState :: Float ->  (Template Relative, Template Absolute) ->  (Template Relative, Template Absolute)
updateTemplateState dt (temp, _absTemp) = (temp {templateObstacles = obstaclesNew}, _absTemp)
  where 
    obstaclesNew = updateStatesObstacles dt (templateObstacles temp)


-- | Update list of active templates given current horizontal speed and time passed
updateTemplates :: Float -> Float -> [Template Relative] -> [Template Relative]
updateTemplates _speed _dt [] = []
updateTemplates speed dt (template : templates) -- = [updateTemplate speed dt template] ++ (map (updateTemplate 0 dt) (take 5 templates)) ++ drop 5 templates
  = updateTemplate speed dt template : relative
    where 
      (relative, _absolute) 
         = unzip (mapWhile False (\(_r, a) -> onScreen a) (updateTemplateState dt) (zip templates (toAbsolute (- screenMaxX) templates)))
      
      mapWhile :: Bool -> (a -> Bool) -> (a -> a) -> [a] -> [a]
      mapWhile foundOnScreenSequence proverka f (x : xs)
       | proverka x = f x : mapWhile True proverka f xs  -- found a sequence that on screen 
       | not foundOnScreenSequence = x : mapWhile False proverka f xs -- before founding a sequence that on screen 
      mapWhile _ _ _ xs = xs -- after founding a sequence that on screen 



-- | Update the world using current horizontal speed and the passed time
-- And handle all it's colliders
updateWorld :: Float -> Float -> WorldState -> WorldState
updateWorld speed dt world = world {worldTemplates = updateTemplates speed dt (worldTemplates world) }

-- Intercation with colliders

-- | Get all the on-screen obstacles and their colliders
getObstacleColliders :: WorldState -> [(Obstacle, Collider)]
getObstacleColliders (WorldState templates) =
  zip obstacles (map getObstacleCollider stateOnObstacles)
  where
    obstacles = (concat . map templateObstacles . getOnScreenTemplates) templates
    stateOnObstacles = filter (\(LazerObstacle (Lazer _ _ state _ _ )) -> state) obstacles